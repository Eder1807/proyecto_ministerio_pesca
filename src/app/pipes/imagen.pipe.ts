import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
    name: 'imagen'
  })
  export class ImagenPipe implements PipeTransform {

    transform( img: string, tipo: string = 'usuarios'): any {

        let url = environment.URL_SERVICIOS + '/upload';

        if ( !img ) {
          return url + '/usuarios/xxx';
        }

        if ( img.indexOf('https') >= 0 ) {
          return img;
        }

        switch ( tipo ) {

            case 'usuarios':
                url += '/usuarios/' + img;
                break;

            default:
                console.log('tipo de imagen no existe, usuario, medicos, hospitales');
                url += '/usuarios/xxx';
          }
        return url;
    }
  }
