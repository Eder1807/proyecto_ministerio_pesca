import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Components/login/login.component';
import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';
import { LoginGuard } from './services/service.index';
import { PagesComponent } from './pages/pages.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent},
  {
      path: '',
      component: PagesComponent,
      canActivate: [ LoginGuard ],
      loadChildren: () => import('./pages/pages.module').then( m => m.PagesModule)
  },
  { path: '**', component: NopagefoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
