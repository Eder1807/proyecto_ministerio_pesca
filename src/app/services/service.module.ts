import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LoginGuard,
         UsuarioService,
         VerificaTokenGuard,
         SubirImagenService } from './service.index';





@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    LoginGuard,
    UsuarioService,
    VerificaTokenGuard,
    SubirImagenService
  ]
})
export class ServiceModule { }
