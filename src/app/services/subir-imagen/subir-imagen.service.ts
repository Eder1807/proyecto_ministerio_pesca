import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { UsuarioService } from '../usuario/usuario.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SubirImagenService {

  constructor(
    public _usuarioService: UsuarioService,
    public http: HttpClient
    ) { }

  subirImagen( archivo: File, tipo: string, id: string ) {
    Swal.fire({
      allowOutsideClick: false,
      icon:'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();
    let url = environment.URL_SERVICIOS + '/upload/' + tipo + '/' + id + '?token=' + this._usuarioService.token;
    let formData = new FormData();
    formData.append( 'imagen', archivo, archivo.name );
    return this.http.put(url, formData);
  }

}
