import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Usuario } from 'src/app/models/usuario.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuario: Usuario;
  token: string;

  constructor(public http: HttpClient, private route: Router) { 
    this.cargarStorage();
  }

  renuevaToken() {

    let url = environment.URL_SERVICIOS + '/login/renuevatoken';
    url += '?token=' + this.token;

    return this.http.get( url ).pipe(
                map( (resp: any) => {

                  this.token = resp.token;
                  localStorage.setItem('token', this.token );
                  console.log('Token renovado');

                  return true;
                }, ( err ) => {
                  this.route.navigate(['/login']);
                  Swal.fire( 'No se pudo renovar token', 'No fue posible renovar token', 'error' );
                  return Observable.throw( err );
                }));


  }

  cargarStorage() {
    if ( localStorage.getItem('token') ) {
      this.token = localStorage.getItem('token');
      this.usuario = JSON.parse( localStorage.getItem('usuario') );
    } else {
      this.token = '';
      this.usuario = null;
    }
  }

  guardarStorage( id: string, token: string, usuario: Usuario) {

    localStorage.setItem('id', id);
    localStorage.setItem('token', token);
    localStorage.setItem('usuario', JSON.stringify(usuario) );

    this.usuario = usuario;
    this.token = token;

  }

  estaLogueado() {
    return ( this.token.length > 5 ) ? true : false;
  }

  logout() {
    this.usuario = null;
    this.token = '';

    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    localStorage.removeItem('id');

    this.route.navigate(['/login']);
  }

  login( usuario: Usuario) {

    const url = environment.URL_SERVICIOS + '/login';
    
    return this.http.post( url, usuario)
    .pipe(
      map( (resp: any) => {

        this.guardarStorage(resp.id, resp.token, resp.usuario);
        return true;

      }));

  }

  cargarUsuarios() {

    const url = environment.URL_SERVICIOS + '/usuario?token=' + this.token;

    return this.http.get( url );

  }

  crearUsuario( usuario ) {

    const url = environment.URL_SERVICIOS + '/usuario?token=' + this.token;
    return this.http.post( url, usuario )
    .pipe(
      map ((resp: any) => {
          Swal.fire('Usuario creado', usuario.email_usuario, 'success');
          return resp.usuario;
    }));
  }

  actualizarUsuario( usuario ) {
    console.log(usuario);
    let url = environment.URL_SERVICIOS + '/usuario/' + usuario.id;
    url += '?token=' + this.token;
    return this.http.put( url, usuario).pipe(
      map( (resp: any) => {

        if ( usuario.id === this.usuario.id ) {
          let usuarioDB: Usuario = resp.usuario;
          this.guardarStorage( usuarioDB.id, this.token, usuarioDB );
        }

        Swal.fire('Usuario actualizado', usuario.nombre, 'success' );

        return resp;
      })
    );
  }

  borrarUsuario( usuarioID ) {
    let url = environment.URL_SERVICIOS + '/usuario/' + usuarioID;
    url += '?token=' + this.token;

    return this.http.delete( url );
  }

}
