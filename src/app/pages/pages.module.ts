import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { PagesRoutingModule } from './pages-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { PipesModule } from '../pipes/pipes.module';
import { PerfilComponent } from './perfil/perfil.component';


@NgModule({
  declarations: [
    DashboardComponent,
    UsuariosComponent,
    PerfilComponent,
  ],
  exports: [
    DashboardComponent,
    UsuariosComponent,
  ],
  imports: [
    DataTablesModule,
    FormsModule,
    SharedModule,
    CommonModule,
    PagesRoutingModule,
    PipesModule,
    ReactiveFormsModule,
  ]
})
export class PagesModule { }
