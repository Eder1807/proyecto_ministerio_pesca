import { Component, OnInit, OnDestroy, ViewChild, TemplateRef } from '@angular/core';
import { Usuario } from 'src/app/models/usuario.model';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import Swal from 'sweetalert2';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UsuarioService } from 'src/app/services/service.index';
import { ModalUploadService } from 'src/app/Components/modal-upload/modal-upload.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit, OnDestroy {

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  // Add Modal
  @ViewChild('template') modal: TemplateRef<any>;
  // Update Modal
  @ViewChild('editTemplate') editmodal: TemplateRef<any>;
  @ViewChild('uploadTemplate') uploadmodal: TemplateRef<any>;

  usuarios$: Observable<Usuario[]>;
  usuarios: Usuario[] = [];
  usuarioForm: FormGroup;
  updateForm: FormGroup;
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {};
  modalMessage: string;
  modalRef: BsModalRef;

  imagenSubir: File;
  imagenTemp: string| ArrayBuffer;

  constructor(private _usuarioService: UsuarioService,
              private formBuilder: FormBuilder,
              private modalService: BsModalService,
              public _modalUploadService: ModalUploadService ) {}

  ngOnInit(): void {
    this.cargarUsuarios();

    this._modalUploadService.notificacion
          .subscribe( resp => this.rerender() );

    this.usuarioForm = this.formBuilder.group({
      nombre_usuario: new FormControl('', [Validators.required]),
      apellido_usuario: new FormControl('', [Validators.required]),
      email_usuario: new FormControl('', [Validators.required, Validators.email]),
      pass_usuario: new FormControl('', [Validators.required, Validators.minLength(8)]),
      user_name: new FormControl('', [Validators.required]),
      rol: new FormControl(null, [Validators.required])
    });
    

    this.updateForm  =  this.formBuilder.group({
      nombre_usuario: new FormControl('', [Validators.required]),
      apellido_usuario: new FormControl('', [Validators.required]),
      email_usuario: new FormControl('', [Validators.required, Validators.email]),
      user_name: new FormControl('', [Validators.required]),
      rol: new FormControl(null, [Validators.required]),
      id: new FormControl('', [Validators.required]),
    });

    this.dtOptions = {
      pagingType: 'full_numbers',
      order: [1, 'asc'],
      columnDefs: [
        { 'width': '13%', 'targets': 6 }
      ],
      language: {
        emptyTable: '',
        zeroRecords: 'No hay coincidencias',
        lengthMenu: 'Mostrar de <select>'+
        '<option value="1">1</option>'+
        '<option value="2">2</option>'+
        '<option value="3">3</option>'+
        '<option value="4">4</option>'+
        '<option value="5">5</option>'+
        '</select> elementos',
        search: 'Buscar:',
        info: 'De _START_ a _END_ de _TOTAL_ elementos',
        infoEmpty: 'De 0 a 0 de 0 elementos',
        infoFiltered: '(filtrados de _MAX_ elementos totales)',
        paginate: {
          first: 'Prim.',
          last: 'Últ.',
          next: 'Sig.',
          previous: 'Ant.'
        }
      }
    };

  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  onAddProduct() {
        this.modalRef = this.modalService.show(this.modal);
    }

 cargarUsuarios() {

    this._usuarioService.cargarUsuarios()
    .subscribe( (resp: any) =>  {
              this.usuarios = resp.usuario;
              this.dtTrigger.next();
            });
  }

  guardarUsusario() {
    this._usuarioService.crearUsuario( this.usuarioForm.value )
        .subscribe( (resp: any) => {
          this.modalRef.hide();
          this.usuarioForm.reset();
          this.rerender();
        }, err => {
          Swal.fire('Ups..!!', 'El email ya existe', 'error');
        });
  }

  rerender() {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.cargarUsuarios();
    });
  }

  showModal(usuario: any) {
    this.updateForm.setValue({
      nombre_usuario: usuario.nombre_usuario,
      apellido_usuario: usuario.apellido_usuario,
      email_usuario: usuario.email_usuario,
      user_name: usuario.user_name,
      rol: usuario.rol.id,
      id: usuario.id,  
    });
    this.modalRef = this.modalService.show(this.editmodal);
  }

  actualizarUsusario() {
    this._usuarioService.actualizarUsuario( this.updateForm.value )
            .subscribe(() => {
              this.modalRef.hide();
              this.rerender();
            },( err)  => {
              Swal.fire( 'Error al actualizar', err.error.errors.message, 'error' );
              this.modalRef.hide();
            });
  }

  eliminar( usuario ) {
    if ( usuario.id === this._usuarioService.usuario.id ) {
      Swal.fire('No puede borrar usuario', 'No se puede borrar a si mismo.', 'error');
      return;
    }
    Swal.fire({
      title: '¿Esta seguro?',
      text: 'Esta a punto de borrar a ' + usuario.nombre_usuario,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.value) {
        this._usuarioService.borrarUsuario( usuario.id )
        .subscribe( (borrado: any) => {
          Swal.fire('Usuario borrado', 'El usuario a sido eliminado correctamente', 'success');
          this.rerender();
        }, err => {
          Swal.fire('Error', 'No se a podido elimnar el usuario', 'error');
        });
      }
    });
  }
  mostrarModal( id: string ) {

    this._modalUploadService.mostrarModal( 'usuarios', id );
  }
}
