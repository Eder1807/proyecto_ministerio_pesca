import { Component, OnInit } from '@angular/core';
import { UsuarioService, SubirImagenService } from 'src/app/services/service.index';

import { Usuario } from 'src/app/models/usuario.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  usuario: Usuario;
  imagenTemp: string | ArrayBuffer;
  imagenSubir: File;
  constructor(public _usuarioService: UsuarioService,
    public _subirArchivo : SubirImagenService) { }

  ngOnInit(): void {
    this.usuario = this._usuarioService.usuario;
  }
  
  actualizar(usuario:Usuario){
    this.usuario.nombre_usuario = usuario.nombre_usuario;
    this.usuario.email_usuario = usuario.email_usuario;
    this.usuario.apellido_usuario = usuario.apellido_usuario;
    this.usuario.foto = usuario.foto;
   
  Swal.fire({
    allowOutsideClick: false,
    icon:'info',
    text: 'Espere por favor...'
  });
  Swal.showLoading();
   this._usuarioService.actualizarUsuario(this.usuario)
   .subscribe();
   
   
  }
  
  seleccionImage( archivo: File ) {

    if ( !archivo ) {
      this.imagenSubir = null;
      return;
    }

    if ( archivo.type.indexOf('image') < 0 ) {
      Swal.fire('Sólo imágenes', 'El archivo seleccionado no es una imagen', 'error');
      this.imagenSubir = null;
      return;
    }

    this.imagenSubir = archivo;

    let reader = new FileReader();
    let urlImagenTemp = reader.readAsDataURL( archivo );

    reader.onloadend = () => this.imagenTemp = reader.result;
  }

  subirImagen() {

    // this._subirArchivoService.subirArchivo( this.imagenSubir, this._modalUploadService.tipo, this._modalUploadService.id )
    //       .then( resp => {

    //         this._modalUploadService.notificacion.emit( resp );
    //         this.cerrarModal();

    //       })
    //       .catch( err => {
    //         Swal.fire('Ups..', 'A ocurrido un error.', 'error');
    //       });
    this._subirArchivo.subirImagen( this.imagenSubir, 'usuarios', this._usuarioService.usuario.id )
          .subscribe( (resp: any) => {
            
            Swal.fire('Actualizada', resp.mensaje, 'success');

          }, err => {
            console.log(err);
            Swal.fire('Ups..', 'A ocurrido un error.', 'error');
          });
  }

}
