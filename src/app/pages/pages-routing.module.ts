import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { VerificaTokenGuard } from '../services/guards/verifica-token.guard';
import { PerfilComponent } from './perfil/perfil.component';

const pagesRoutes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [ VerificaTokenGuard ],
        data: { titulo: 'Dashboard', pages: 'Principal' }
    },
    { path: 'usuarios', component: UsuariosComponent, data: { titulo: 'Usuarios', pages: 'Mantenimiento' } },
    { path: 'perfil', component: PerfilComponent, data: { titulo: 'Perfil', pages: 'Mantenimiento'} },
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
];


@NgModule({
    imports: [RouterModule.forChild(pagesRoutes)],
    exports: [RouterModule]
})

export class PagesRoutingModule { }