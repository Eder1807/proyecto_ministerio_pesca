import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario.model';
import { NgForm } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  cargar = true;

  constructor(public router: Router, private _usuarioService: UsuarioService) { }

  ngOnInit(): void {
  }

  ingresar(forma: NgForm) {

    this.cargar = false;

    if ( forma.invalid ) {
      return;
    }

    const usuario = new Usuario( null, null, forma.value.email, forma.value.password, null );
    console.log(usuario);

    this._usuarioService.login( usuario )
              .subscribe( () => {
                this.router.navigate(['/dashboard']);
                this.cargar = true;
              }, err => {
                Swal.fire( 'Error al iniciar sesion', err.error.mensaje, 'error' );
                this.cargar = true;
              });
  }


}
