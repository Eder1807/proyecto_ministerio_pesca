import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-especie',
  templateUrl: './modal-especie.component.html',
  styleUrls: ['./modal-especie.component.css']
})
export class ModalEspecieComponent implements OnInit {
  text:any = "Registrar Nueva Especie";
  constructor() { }

  ngOnInit(): void {
  }

}
