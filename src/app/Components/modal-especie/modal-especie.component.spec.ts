import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEspecieComponent } from './modal-especie.component';

describe('ModalEspecieComponent', () => {
  let component: ModalEspecieComponent;
  let fixture: ComponentFixture<ModalEspecieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEspecieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEspecieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
