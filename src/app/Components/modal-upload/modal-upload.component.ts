import { Component, OnInit } from '@angular/core';
import { ModalUploadService } from './modal-upload.service';
import { SubirImagenService } from '../../services/subir-imagen/subir-imagen.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-upload',
  templateUrl: './modal-upload.component.html',
  styleUrls: ['./modal-upload.component.css']
})
export class ModalUploadComponent implements OnInit {

  imagenSubir: File;
  imagenTemp: string | ArrayBuffer;
  
  constructor(
    public _subirArchivoService: SubirImagenService,
    public _modalUploadService: ModalUploadService
  ) { }

  ngOnInit(): void {
  }

  cerrarModal() {
    this.imagenTemp = null;
    this.imagenSubir = null;
    this._modalUploadService.ocultarModal();
  }

  seleccionImage( archivo: File ) {

    if ( !archivo ) {
      this.imagenSubir = null;
      return;
    }

    if ( archivo.type.indexOf('image') < 0 ) {
      Swal.fire('Sólo imágenes', 'El archivo seleccionado no es una imagen', 'error');
      this.imagenSubir = null;
      return;
    }

    this.imagenSubir = archivo;

    let reader = new FileReader();
    let urlImagenTemp = reader.readAsDataURL( archivo );

    reader.onloadend = () => this.imagenTemp = reader.result;

  }

  subirImagen() {

    // this._subirArchivoService.subirArchivo( this.imagenSubir, this._modalUploadService.tipo, this._modalUploadService.id )
    //       .then( resp => {

    //         this._modalUploadService.notificacion.emit( resp );
    //         this.cerrarModal();

    //       })
    //       .catch( err => {
    //         Swal.fire('Ups..', 'A ocurrido un error.', 'error');
    //       });
    this._subirArchivoService.subirImagen( this.imagenSubir, this._modalUploadService.tipo, this._modalUploadService.id )
          .subscribe( (resp: any) => {

            this._modalUploadService.notificacion.emit( resp );
            this.cerrarModal();
            Swal.fire('Actualizada', resp.mensaje, 'success');

          }, err => {
            console.log(err);
            Swal.fire('Ups..', 'A ocurrido un error.', 'error');
          });

  }

}
