export class Usuario {

    constructor(
        public nombre_usuario: string,
        public apellido_usuario: string,
        public email_usuario: string,
        public pass_usuario: string,
        public user_name: string,
        public foto?: string,
        public rol?: Rol,
        public id?: string
    ) {}

}

export class Rol {

    constructor(
        public role: string
    ) {}
}
