import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NabvarComponent } from './nabvar/nabvar.component';
import { NavbarHorizontalComponent } from './navbar-horizontal/navbar-horizontal.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';
import { ModalUploadComponent } from '../Components/modal-upload/modal-upload.component';
import { PipesModule } from '../pipes/pipes.module';



@NgModule({
  declarations: [
    NabvarComponent,
    NavbarHorizontalComponent,
    SidebarComponent,
    FooterComponent,
    HeaderComponent,
    NopagefoundComponent,
    ModalUploadComponent
  ],
  exports: [
    NabvarComponent,
    NavbarHorizontalComponent,
    SidebarComponent,
    FooterComponent,
    HeaderComponent,
    ModalUploadComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    PipesModule
  ]
})
export class SharedModule { }
